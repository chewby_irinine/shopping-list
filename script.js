let shoppingCardsStorageData = [];
let nameField = document.getElementById("name");
let amountField = document.getElementById("amount");
let priceField = document.getElementById("price");
let dateField = document.getElementById("date");
let list = document.getElementById("list");
let filterRadio = document.getElementsByName("filter_radio");
let filterNameField = document.getElementById("filter_name");
let filterPriceFromField = document.getElementById("filter_price_from");
let filterPriceToField = document.getElementById("filter_price_to");
let select = document.getElementById("select");

try {
	if (localStorage.getItem("shoppingCardsStorageData") !== null) {
		shoppingCardsStorageData = JSON.parse(
			localStorage.getItem("shoppingCardsStorageData")
		);
		renderList(shoppingCardsStorageData, list);
	} else {
		localStorage.setItem(
			"shoppingCardsStorageData",
			JSON.stringify(shoppingCardsStorageData)
		);
	}
} catch (e) {
	alert("Ошибка преобразования данных: ", e);
}

document.getElementById("add").addEventListener("click", function (e) {
	e.preventDefault();

	try {
		if (nameField.value !== "") {
			if (
				/^\d{4}\-\d{2}\-\d{2}$/.test(dateField.value) ||
				dateField.value === ""
			) {
				if (
					!shoppingCardsStorageData.find(
						(el) => el.name === nameField.value
					)
				) {
					shoppingCardsStorageData.push({
						id: shoppingCardsStorageData.reduce((max, el) => {
							return max >= el.id ? ++max : el.id;
						}, 0),
						name: nameField.value,
						amount:
							amountField.value === "" ? null : amountField.value,
						price:
							priceField.value === "" ? null : priceField.value,
						date: dateField.value === "" ? null : dateField.value,
					});

					localStorage.setItem(
						"shoppingCardsStorageData",
						JSON.stringify(shoppingCardsStorageData)
					);

					nameField.value = "";
					amountField.value = "";
					priceField.value = "";
					dateField.value = "";

					renderList(shoppingCardsStorageData, list);

					console.log(shoppingCardsStorageData);
				} else {
					throw new Error("Нельзя добавить дубликат!");
				}
			} else {
				throw new Error("Неверный формат даты!");
			}
		} else {
			throw new Error("Отсутствуют данные, введите название!");
		}
	} catch (e) {
		alert(e);
	}
});

filterRadio[0].addEventListener("change", function (e) {
	e.preventDefault();

	filterPriceFromField.value = "";
	filterPriceToField.value = "";
	filterPriceFromField.style.display = "none";
	filterPriceToField.style.display = "none";
	filterNameField.style.display = "inline";

	renderList(combinationFilter(), list);
});

filterRadio[1].addEventListener("change", function (e) {
	e.preventDefault();

	filterNameField.value = "";
	filterNameField.style.display = "none";
	filterPriceFromField.style.display = "inline";
	filterPriceToField.style.display = "inline";

	renderList(combinationFilter(), list);
});

filterNameField.addEventListener("input", function (e) {
	e.preventDefault();

	renderList(combinationFilter(), list);
});

filterPriceFromField.addEventListener("input", function (e) {
	e.preventDefault();

	renderList(combinationFilter(), list);
});

filterPriceToField.addEventListener("input", function (e) {
	e.preventDefault();

	renderList(combinationFilter(), list);
});

select.addEventListener("change", function (e) {
	e.preventDefault();

	renderList(combinationFilter(), list);
});

function renderList(data, list) {
	list.innerHTML = "";

	data.forEach((el) => {
		list.innerHTML += `<li class="list__item">
                <span class="list__field">${el.name}</span>
                <span class="list__field">${
					el.amount !== null ? el.amount + " шт." : ""
				}</span>
                <span class="list__field">${
					el.price !== null ? el.price + " $" : ""
				}</span>
                <span class="list__field">${el.date ?? ""}</span>
                <button 
                    type="button" 
                    class="btn 
                    btn-outline-danger
                    list__button"
                    onclick="deleteItem(${el.id})">
                    <i class="far fa-trash-alt"></i>
                    Delete
                </button>
            </li>`;
	});
}

function filterByName(text, data) {
	return data.filter((el) => el.name.indexOf(text) !== -1);
}

function filterByPrice(from, to, data) {
	return data
		.filter((el) => +el.price >= (from !== "" ? from : 0))
		.filter((el) => +el.price <= (to !== "" ? to : Infinity));
}

function combinationFilter() {
	switch (select.value) {
		case "amount":
			return filterByName(
				filterNameField.value,
				filterByPrice(
					filterPriceFromField.value,
					filterPriceToField.value,
					shoppingCardsStorageData
				)
			).sort((a, b) => {
				return +(a.amount ?? Infinity) >= +(b.amount ?? Infinity)
					? 1
					: -1;
			});

		case "price":
			return filterByName(
				filterNameField.value,
				filterByPrice(
					filterPriceFromField.value,
					filterPriceToField.value,
					shoppingCardsStorageData
				)
			).sort((a, b) => {
				return +(a.price ?? Infinity) >= +(b.price ?? Infinity)
					? 1
					: -1;
			});

		case "date":
			return filterByName(
				filterNameField.value,
				filterByPrice(
					filterPriceFromField.value,
					filterPriceToField.value,
					shoppingCardsStorageData
				)
			).sort((a, b) => {
				return (a.date ?? ":") >= (b.date ?? ":") ? 1 : -1;
			});

		default:
			return filterByName(
				filterNameField.value,
				filterByPrice(
					filterPriceFromField.value,
					filterPriceToField.value,
					shoppingCardsStorageData
				)
			).sort((a, b) => {
				return a.id >= b.id ? 1 : -1;
			});
	}
}

function deleteItem(id) {
	shoppingCardsStorageData.splice(
		shoppingCardsStorageData.indexOf(
			shoppingCardsStorageData.find((el) => el.id === id)
		),
		1
	);
	localStorage.setItem(
		"shoppingCardsStorageData",
		JSON.stringify(shoppingCardsStorageData)
	);
	renderList(shoppingCardsStorageData, list);
}
